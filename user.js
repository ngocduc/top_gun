import axios from 'axios';

class User {
  constructor(id = 2, name = 'nnnnn', age = 33, email = 'dnd@fmail.com') {
    this.id = id;
    this.name = name;
    this.age = age;
    this.email = email;
  }

  infoUser(infoName) {
    if (Object.keys(this).includes(infoName)) {
      return this[infoName];
    }

    return this;
  }

  set info({ id, name, age, email }) {
    const info = { ...{ id, name, age, email } };
    Object.keys(info).forEach((e) => {
      if (info[e] !== undefined) this[e] = info[e];
    });

    return this;
  }

  async refeshInfo(uid) {
    if (!uid) throw new Error('need param...');
    try {
      const response = await axios.get(`http://www.json-generator.com/api/json/get/bUPFMuvrkO?indent=${uid}`);
      console.log(response.data, `http://www.json-generator.com/api/json/get/bUPFMuvrkO?indent=${uid}`);
      const data = response.data;
      if (Object.keys(data).includes('id')) throw new Error('data structure is err...');
      this.info = data;

      return this;
    } catch (err) {
      throw new Error('Request failed with status code 500');
    }
  }
}

// const dnd = new User(1, 2, 3, 4);
// dnd.refeshInfo('fsf').then(_ => {
//   console.log(dnd.infoUser().infoUser());
// })
// dnd.info = { id: 122 };

export default User;
// http://www.json-generator.com/api/json/get/bUPFMuvrkO?indent=2
