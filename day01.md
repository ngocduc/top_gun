# hoisting
` khái niêm: đưa khai bào báo biến hoặc function declaration lên đầu  function hoặc scrop `
* khai bao biến và function thì function đc ưu tiên và ghi đè biến 
```js
function dnd () {console.log( 'dnd ')}
var dnd ;

console.log( dnd ); // function 
```
* khi khai báo và gán thì hoi lên rồi nó ghi dè cái cuối 
```js
var dnd = 1 ;
function dnd () {console.log( 'dnd ')}
console.log( dnd ); // function 
```

# closure ( bao đóng )
` khái niêm về inner function và outter fucntion `
* là function bên trong 1 function khác và function đó có khả năng nhớ vị trí mà nó được khai báo 
* nó nhớ outter function ( môi trương nơi nó đc khai báo ) cho đến khi chương tình tắt 
```js
function dnd () {
    let a = 0;
    function add() {
        return ++a;
    }
    return add;
}

let dd = dnd();
console.log(dd());
console.log(dd());
console.log(dd());

function ahihi (cb) {
    let a = 10;
    console.log(cd())
}

ahihi(dd);
// 1,2,3,4
```