import axios from 'axios';
import User from '../user';
jest.mock('axios');
describe('i want test class User', () => {
  describe('i want test method infoRefest', () => {
    let user;
    beforeAll(() => {
      user = new User();
    });
    afterEach(() => {
      axios.get.mockRestore();
    });
    test('1. return',  () => {
      const dataRes = {
        data: { email: "dnd@fmail.com", age: 23, id: 1, name: "ducdn"}
      };
      axios.get.mockResolvedValue(dataRes);
      return  expect(user.refeshInfo(1)).resolves.toEqual(dataRes.data);
    })

    test('2. err when param is undefined', async () => {
      return  expect(user.refeshInfo()).rejects.toEqual(new Error('need param...'));
    })

    test('3. khi ko co id', async () => {
      const err500 = new Error('Request failed with status code 500');
      axios.get.mockResolvedValue(err500);
      return  expect(user.refeshInfo('adsgfs')).rejects.toEqual(err500);
    })
  })
});
